var publishItems = {
		'Master': [],
		'MaxTest': [],
		'Action': [],
		'SiteScript': [],
		'Page': [],
		'Site pages': []
	},
	publishInstuctions = 'PI\n';

$('.publish-selection .campaign-card:not(.ng-hide) td[class*="content"] .text-overflow:first-child').each(function(){
	var itemType = $.trim($(this).next('.text-overflow').text()),
		itemValue = $.trim($(this).text());

	if(publishItems.hasOwnProperty(itemType)){
		publishItems[itemType].push(itemValue);
	}
});

for(var p in publishItems){
	if(publishItems.hasOwnProperty(p) && publishItems[p].length){
		if(p !== 'Site pages'){
			publishInstuctions += p + 's:\n';
			publishItems[p].forEach(function(item){
				publishInstuctions += '- ' + item + '\n';
			});
		} else {
			publishInstuctions += publishItems[p][0];
		}
	}
}

console.info(publishInstuctions);
if( typeof window.copy === 'function' ){
	window.copy(publishInstuctions);
}

